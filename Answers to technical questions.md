1. How long did you spend on the technical test? What would you add to your solution if you had more time? If you didn't spend much time on the technical test then use this as an opportunity to explain what you would add.
- i spent only 12 dedicated hours cause we had unplanned release with many new userstories
- after that I had the opportunity to have more time to work on it for more 12 hours
- i would do the below if i have more time
    - Handle the recyclerView in a different way not depending on the text
    - Read data from file as i implemented in my current project
2. What do you think is the most interesting trend in test automation?
- using native frameworks since it will provide speed and also team integration since the quality of deliverables will be shared among the whole team
3. How would you implement test automation in a legacy application? Have you ever had to do this?
- i would implement it on a minimum scale since it might be changed
4. How would you improve the customer experience of the Just Eat website?
- i have not been using it for so many times yet i would think that it needs some alerts for example when you try to select from different restaurants you should be notified for losing items in cart
5. Please describe yourself using JSON. <br/>
{
    "name": "Hussein",
    "lastname": "Ibrahim",
    "hobbies": [
                "Running",
                "Hiking",
                "Music"
                ],
    "techSkills": [
                "QA",
                "Mobile Testing",
                "Appium",
                "Espresso",
                "XCTest",
                "Java",
                "Kotlin",
                "Bitrise"
                ],
    "personalSkills": [
                "Communication",
                "Team Player",
                "Handle work load",
                "Eager to learn",
                "Love challenges"
              ]
}