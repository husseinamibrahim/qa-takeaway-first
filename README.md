1. Unzip the project folder and open it in android studio
2. Install Gherkin and Cucumber for kotlin plugins
3. Sync project gradle
4. Make sure the emulator is on or the real device is attached and animation is turned off
5. To run specific test type the below command in terminal (Check the tag @test1 in the feature file for each test and update the command accordingly) <br/>
    ./gradlew connectedCheck -Pcucumber -Ptags="@test1"
6. To run all tests type the below command <br/>
    ./gradlew connectedCheck -Pcucumber -Ptags="@all"
