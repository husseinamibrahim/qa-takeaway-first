package com.justeat.androiduitechtest.cucumber.steps

import android.support.test.espresso.NoMatchingViewException
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import com.justeat.androiduitechtest.cucumber.robots.AddTaskRobot
import cucumber.api.java.Before
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.hamcrest.Matchers.not

/**
 * All test steps to be used in different tests based on the feature files
 * Each method represents a step in a scenario
 */

class AddTaskSteps {

    val addTaskRobot =
        AddTaskRobot()

    @Before
    fun setUp(){
        addTaskRobot.launchApp()
    }

    @Given("^User starts the application$")
    fun startApp(){
        addTaskRobot.toolBarText.check(matches(isDisplayed()))

    }

    @When("^User clicks on add task floating button$")
    fun clickOnAddTaskFloatingBtn(){
        addTaskRobot.clickOnAddBtn()
    }

    @And("^User Enters task name$")
    fun enterTaskName(){
        addTaskRobot.typeTextInTextView()
    }

    @And("^User clicks on add task$")
    fun clickOnAddTask(){
        addTaskRobot.clickOnPopUpAddBtn()
    }

    @Then("^Task is displayed in active tasks$")
    fun taskIsDisplayed(){
        addTaskRobot.addedTaskItem.check(matches(isDisplayed()))
    }

    @And("^User clicks on task from active tasks$")
    fun clickOnTaskFromActiveTasks(){
        addTaskRobot.addedTaskItem.perform(click())
    }

    @And("^User clicks on complete from the task action menu$")
    fun clickOnCompleteTask(){
        addTaskRobot.completeTaskBtn.check(matches(isDisplayed()))
        addTaskRobot.completeTaskBtn.perform(click())
    }

    @Then("^Task is removed from active tasks tab and moved to completed tasks tab$")
    fun taskIsRemovedFromActiveTasks(){
        addTaskRobot.addedTaskItem.check(matches(not(isDisplayed())))
        addTaskRobot.completedTasksText.perform(click())
        addTaskRobot.addedTaskItem.check(matches(isDisplayed()))
    }

    @And("^User clicks on delete from the task action menu$")
    fun clickOnDeleteTask(){
        addTaskRobot.deleteTaskBtn.check(matches(isDisplayed()))
        addTaskRobot.deleteTaskBtn.perform(click())
    }

    @Then("^Task is removed from active tasks tab$")
    fun taskIsDeletedFromActiveTasks(){
        try {
            addTaskRobot.addedTaskItem.check(matches(not(isDisplayed())))
        } catch (e: NoMatchingViewException){

        }
    }

}