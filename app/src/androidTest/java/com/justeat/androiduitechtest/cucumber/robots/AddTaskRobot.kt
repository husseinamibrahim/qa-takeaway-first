package com.justeat.androiduitechtest.cucumber.robots

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import com.justeat.androiduitechtest.MainActivity
import com.justeat.androiduitechtest.R
import org.hamcrest.Matchers.allOf

/**
 * Robots approach to add methods to perform actions on pages
 * All elements are being located and created here
 * Common methods are created here
 */
class AddTaskRobot {

    val mActivityRule = ActivityTestRule(MainActivity::class.java, false, false)
    private lateinit var launchedActivity: MainActivity

    val addBtn = onView(withId(R.id.add))
    val toolBarText = onView(allOf(withText("AndroidUITechTest")))
    val activeTasksText = onView(allOf(withText("ACTIVE TASKS")))
    val completedTasksText = onView(allOf(withText("COMPLETED TASKS")))
    val recyclerViewTasksList = onView(withId(R.id.list))
    val itemElementText = "Test Case 1"
    val addedTaskItem = onView(withText(itemElementText))
    val addTaskTextField = onView(withId(R.id.add_task))
    val addTaskBtn = onView(allOf(withText("Add")))
    val completeTaskBtn = onView(withId(R.id.task_options_complete))
    val deleteTaskBtn = onView(withId(R.id.task_options_delete))



    fun launchApp(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        launchedActivity = mActivityRule.launchActivity(intent)
    }

    fun clickOnAddBtn(){
        addBtn.perform(click())
    }

    fun typeTextInTextView(){
        addTaskTextField.perform(click())
        addTaskTextField.perform(typeText("Test Case 1"))
    }

    fun clickOnPopUpAddBtn(){
        addTaskBtn.perform(click())
    }

    fun clickOnItemOnTheList(){
        recyclerViewTasksList.perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
    }
}