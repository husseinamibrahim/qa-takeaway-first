package com.justeat.androiduitechtest.cucumber.runner

import android.os.Bundle
import android.support.test.runner.AndroidJUnitRunner
import com.justeat.androiduitechtest.BuildConfig
import cucumber.api.android.CucumberInstrumentationCore

/**
 * Custom cucumber runner to load the scenarios and tags
 */
class CucumberTestRunner : AndroidJUnitRunner() {
    companion object {
        const val CUCUMBER_TAGS_KEY = "tags"
        const val CUCUMBER_SCENARIO_KEY = "name"
    }

    private val instrumentationCore = CucumberInstrumentationCore(this)

    override fun onCreate(arguments: Bundle?) {
        val tags = BuildConfig.TEST_TAGS
        var scenario = BuildConfig.TEST_SCENARIO

        if (tags.isNotEmpty()) {
            arguments?.putString(
                CUCUMBER_TAGS_KEY,
                tags.replace("\\s".toRegex(), "")
            )
        }

        if (scenario.isNotEmpty()) {
            scenario = scenario.replace(" ".toRegex(), "\\\\s")
            arguments?.putString(CUCUMBER_SCENARIO_KEY, scenario)
        }

        instrumentationCore.create(arguments)
        super.onCreate(arguments)
    }

    override fun onStart() {
        waitForIdleSync()
        instrumentationCore.start()
    }
}