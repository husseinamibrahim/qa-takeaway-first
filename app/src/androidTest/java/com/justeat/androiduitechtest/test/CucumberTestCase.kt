package com.justeat.androiduitechtest.test

import cucumber.api.CucumberOptions

/**
 * Default cucumber runner
 * Linking the features package to the steps definition package
 */
@CucumberOptions(
    features = ["features"],
    glue = ["com.justeat.androiduitechtest.cucumber.steps"],
    tags = ["@test1","@test2","@test3","@all"],
    plugin = ["pretty"]
)
class CucumberTestCase