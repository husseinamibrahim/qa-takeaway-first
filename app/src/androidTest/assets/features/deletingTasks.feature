Feature: Completing tasks
  @test3
    @all
  Scenario: User deleting task
    Given User starts the application
    When User clicks on add task floating button
    And User Enters task name
    And User clicks on add task
    And User clicks on task from active tasks
    And User clicks on delete from the task action menu
    Then Task is removed from active tasks tab