Feature: Completing tasks
  @test2
    @all
  Scenario: User completing task
    Given User starts the application
    When User clicks on add task floating button
    And User Enters task name
    And User clicks on add task
    And User clicks on task from active tasks
    And User clicks on complete from the task action menu
    Then Task is removed from active tasks tab and moved to completed tasks tab